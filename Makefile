composer-install:
	composer install

load-fixtures:
	bin/console doctrine:fixtures:load -n --append

db-reload:
	bin/console doctrine:database:drop --if-exists --force
	bin/console doctrine:database:create
	bin/console doctrine:migration:migrate --no-interaction

install-assets:
	yarn install
	yarn encore dev

init: composer-install db-reload load-fixtures install-assets

install:
	composer install --no-dev --optimize-autoloader
	bin/console doctrine:database:create --if-not-exists
	bin/console doctrine:migration:migrate --no-interaction
	yarn install
	yarn encore prod
	bin/console cache:warmup

phpstan:
	vendor/bin/phpstan analyse -c phpstan.neon

php-cs-fix:
	vendor/bin/php-cs-fixer fix --allow-risky=yes

lint: php-cs-fix

xdebug-on:
	phpenmod xdebug
	service php7.4-fpm reload

xdebug-off:
	phpdismod xdebug
	service php7.4-fpm reload

psalm:
	./vendor/bin/psalm