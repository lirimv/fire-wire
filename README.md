## Installation
1. `docker-compose up -d`
2. `./container make init`
3. `http::/localhost:3001`
## Email Import
CLI: `php bin/console app:import <eingangs verzeichnis> <ausgangs verzeichnis>`

*Beispiel* `php bin/console app:import data/in/ data/out/`

## Utils
Style check:
`./container make lint`