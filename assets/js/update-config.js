const $ = require('jquery');

$(".custom-input").change(function() {
    if(this.checked) {
        var data = $(this).attr("name").split("-")
        var value = $(this).attr("value")
        var id = data[0]
        var column = data[1]

        $.post("/admin/update-matrix",
            {
                id: id,
                column: column,
                value: value
            },
            function(data, status){
                alert("Data: " + data + "\nStatus: " + status);
            });
    }
});

$(".custom-input-menschen-leben").change(function() {
    if(this.checked) {
        const value = $(this).attr("value");

        $.post("/admin/update-config",
            {
                value: value
            },
            function(data, status){
                alert("Data: " + data + "\nStatus: " + status);
            });
    }
});