<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Import\Email\Importer;
use DomainException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Webmozart\Assert\Assert;

final class ImportCommand extends Command
{
    private Importer $emailImporter;

    public function __construct(Importer $emailImporter)
    {
        $this->emailImporter = $emailImporter;

        parent::__construct();
    }

    protected static $defaultName = 'app:import';

    protected function configure(): void
    {
        $this
            ->setDescription('Importiert neue Alarmnachrichten')
            ->addArgument('inDirectory', InputArgument::REQUIRED, 'Verzeichnis mit den Alarmnachrichten')
            ->addArgument('outDirectory', InputArgument::REQUIRED, 'Verzeichnis nach der Verarbeitung');
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $inDirectory = $input->getArgument('inDirectory');
        $outDirectory = $input->getArgument('outDirectory');

        if (!is_string($inDirectory) || !is_string($outDirectory)) {
            throw new DomainException('Invalid arguments provided!');
        }

        Assert::directory($inDirectory);
        Assert::directory($outDirectory);

        $this->emailImporter->process($inDirectory, $outDirectory);

        return 0;
    }
}
