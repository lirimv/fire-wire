<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\AlarmNachricht;
use App\Entity\Configuration;
use App\Entity\StichwortMatrix as DomainStichwortMatrix;
use App\Form\AlarmnachrichtType;
use App\Models\Config;
use App\Models\Einsatz as Model;
use App\Models\StichwortMatrix;
use App\Repository\AlarmNachrichtRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\EinsatzRepository;
use App\Repository\StichwortMatrixRepository;
use App\Service\Validator\Decider;
use DateTimeImmutable;
use DomainException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/admin")
 * @IsGranted("ROLE_ADMIN")
 */
final class AdminController extends AbstractController
{
    private EinsatzRepository $einsatzRepository;
    private AlarmNachrichtRepository $alarmnachrichtRepository;
    private Decider $decider;
    private StichwortMatrixRepository $stichwortMatrixRepository;
    private ConfigurationRepository $configRepository;

    public function __construct(
        EinsatzRepository $einsatzRepository,
        AlarmNachrichtRepository $alarmNachrichtRepository,
        Decider $decider,
        StichwortMatrixRepository $stichwortMatrixRepository,
        ConfigurationRepository $configRepository
    ) {
        $this->einsatzRepository = $einsatzRepository;
        $this->alarmnachrichtRepository = $alarmNachrichtRepository;
        $this->decider = $decider;
        $this->stichwortMatrixRepository = $stichwortMatrixRepository;
        $this->configRepository = $configRepository;
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction(): Response
    {
        $einsaetze = $this->einsatzRepository->findAll();

        $models = [];
        foreach ($einsaetze as $key => $einsatz) {
            $models[$key]['sichtbarkeit'] = $this->decider->decide($einsatz);
            $models[$key]['nachricht'] = Model::createKomplett($einsatz);
        }

        return $this->render('admin/index.html.twig', [
            'einsaetze' => $models,
        ]);
    }

    /**
     * @Route("/input", name="input")
     */
    public function inputAction(Request $request): Response
    {
        $alarmnachricht = AlarmNachricht::createEmpty();
        $form = $this->createForm(AlarmnachrichtType::class, $alarmnachricht);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $alarmnachricht = $form->getData();

            if (!$alarmnachricht instanceof AlarmNachricht) {
                throw new DomainException('Wrong type received!');
            }
            $alarmnachricht->setAlamierungszeit(new DateTimeImmutable('now'));
            $this->alarmnachrichtRepository->save($alarmnachricht);

            return $this->redirectToRoute('input');
        }

        return $this->render('admin/input.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/einsatz/{einsatznummer}/schliessen", name="einsatz_schliessen", methods={"GET"})
     */
    public function einsatzSchliessen(int $einsatznummer): Response
    {
        $alarmnachricht = $this->alarmnachrichtRepository->findOneBy(['einsatznummer' => $einsatznummer]);

        if (null === $alarmnachricht) {
            throw new \LogicException('Einsatz nicht gefunden');
        }

        $alarmnachricht->getEinsatz()->setStatus('geschlossen');
        $this->einsatzRepository->save($alarmnachricht->getEinsatz());

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/config", name="config", methods={"GET"})
     */
    public function configAction(): Response
    {
        $matrix = $this->stichwortMatrixRepository->findAll();
        $config = $this->configRepository->find(1);

        Assert::allIsInstanceOf($matrix, DomainStichwortMatrix::class);
        Assert::isInstanceOf($config, Configuration::class);

        $matrixModel = array_map([StichwortMatrix::class, 'fromEntity'], $matrix);
        $configModel = Config::fromEntity($config);

        return $this->render('admin/config.html.twig', [
            'matrix' => $matrixModel,
            'config' => $configModel,
        ]);
    }

    /**
     * @Route("/update-matrix", name="update-matrix", methods={"POST"})
     */
    public function updateMatrixAction(Request $request): JsonResponse
    {
        $id = (int) $request->get('id');
        $column = $request->get('column');
        $value = $request->get('value');

        $result = null;
        switch ($value) {
            case 'true':
                $result = true;
                break;
            case 'false':
                $result = false;
                break;
            case 'null':
                $result = null;
                break;
        }

        $stichwort = $this->stichwortMatrixRepository->find(['id' => $id]);
        Assert::isInstanceOf($stichwort, DomainStichwortMatrix::class);

        switch ($column) {
            case 'innerorts':
                $stichwort->setInnerorts($result);
                break;
            case 'ausserorts':
                $stichwort->setAusserorts($result);
                break;
            case 'wasser':
                $stichwort->setWasser($result);
                break;
        }

        $this->stichwortMatrixRepository->save($stichwort);

        return new JsonResponse(JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/update-config", name="update-config", methods={"POST"})
     */
    public function updateConfig(Request $request): JsonResponse
    {
        $config = $this->configRepository->find(1);
        Assert::isInstanceOf($config, Configuration::class);

        $value = (bool) $request->get('menschenLeben');

        $config->setMenschenLeben($value);
        $this->configRepository->save($config);

        return new JsonResponse(JsonResponse::HTTP_NO_CONTENT);
    }
}
