<?php

declare(strict_types=1);

namespace App\Controller;

use App\Models\Einsatz;
use App\Repository\EinsatzRepository;
use App\Service\Validator\Decider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HomeController extends AbstractController
{
    private EinsatzRepository $einsatzRepository;
    private Decider $decider;

    public function __construct(EinsatzRepository $einsatzRepository, Decider $decider)
    {
        $this->einsatzRepository = $einsatzRepository;
        $this->decider = $decider;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $einsaetze = $this->einsatzRepository->getOffeneEinsaetze();

        $models = [];
        foreach ($einsaetze as $einsatz) {
            $desicion = $this->decider->decide($einsatz);
            $models[] = Einsatz::create($desicion, $einsatz);
        }

        return $this->render('home/index.html.twig', [
            'einsaetze' => $models,
        ]);
    }
}
