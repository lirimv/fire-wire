<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseRepository")
 */
class Adresse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $strasse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $hausnummer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $ortsteil;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $ort;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $objekt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AlarmNachricht", mappedBy="adresse")
     */
    private Collection $alarmNachrichten;

    public function __construct(?string $strasse, ?string $hausnummer, ?string $ortsteil, ?string $ort, ?string $objekt)
    {
        $this->strasse = $strasse ?? '';
        $this->hausnummer = $hausnummer ?? '';
        $this->ortsteil = $ortsteil ?? '';
        $this->ort = $ort ?? '';
        $this->objekt = $objekt ?? '';
        $this->alarmNachrichten = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStrasse(): string
    {
        return $this->strasse;
    }

    public function setStrasse(string $strasse): self
    {
        $this->strasse = $strasse;

        return $this;
    }

    public function getHausnummer(): string
    {
        return $this->hausnummer;
    }

    public function setHausnummer(?string $hausnummer): self
    {
        $this->hausnummer = $hausnummer ?? '';

        return $this;
    }

    public function getOrtsteil(): string
    {
        return $this->ortsteil;
    }

    public function setOrtsteil(string $ortsteil): self
    {
        $this->ortsteil = $ortsteil;

        return $this;
    }

    public function getOrt(): string
    {
        return $this->ort;
    }

    public function setOrt(string $ort): self
    {
        $this->ort = $ort;

        return $this;
    }

    public function getObjekt(): string
    {
        return $this->objekt;
    }

    public function setObjekt(?string $objekt): self
    {
        $this->objekt = $objekt ?? '';

        return $this;
    }

    /**
     * @return AlarmNachricht[]
     */
    public function getAlarmNachrichten(): iterable
    {
        return $this->alarmNachrichten;
    }

    public function addAlarmNachrichten(AlarmNachricht $alarmNachrichten): self
    {
        if (!$this->alarmNachrichten->contains($alarmNachrichten)) {
            $this->alarmNachrichten[] = $alarmNachrichten;
            $alarmNachrichten->setAdresse($this);
        }

        return $this;
    }

    public function removeAlarmNachrichten(AlarmNachricht $alarmNachrichten): self
    {
        if ($this->alarmNachrichten->contains($alarmNachrichten)) {
            $this->alarmNachrichten->removeElement($alarmNachrichten);
        }

        return $this;
    }
}
