<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlarmNachrichtRepository")
 */
class AlarmNachricht
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private string $einsatznummer = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $einsatzstelle_zusatz = '';

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private ?string $einsatzstelle_bemerkung = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $koordinaten = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Adresse", inversedBy="alarmNachrichten", cascade={"persist"})
     * @ORM\JoinColumn(name="adresse_id", referencedColumnName="id")
     */
    private Adresse $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private string $alarm_stichwort = '';

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $timestamp;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private ?string $meldebild = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $alamierungszeit;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private ?string $bemerkung = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private ?string $meldender = null;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private ?string $einsatzmittel = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Einsatz", inversedBy="alarmNachricht", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private Einsatz $einsatz;

    public function __construct(Adresse $adresse)
    {
        $this->adresse = $adresse;
        $this->einsatz = new Einsatz($this);
        $this->timestamp = new DateTimeImmutable('now');
    }

    public static function createEmpty(): self
    {
        return new self(
            new Adresse('', '', '', '', '')
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEinsatznummer(): string
    {
        return $this->einsatznummer;
    }

    public function setEinsatznummer(string $einsatznummer): self
    {
        $this->einsatznummer = $einsatznummer;

        return $this;
    }

    public function getEinsatzstelleZusatz(): ?string
    {
        return $this->einsatzstelle_zusatz;
    }

    public function setEinsatzstelleZusatz(?string $einsatzstelle_zusatz): self
    {
        $this->einsatzstelle_zusatz = $einsatzstelle_zusatz;

        return $this;
    }

    public function getEinsatzstelleBemerkung(): ?string
    {
        return $this->einsatzstelle_bemerkung;
    }

    public function setEinsatzstelleBemerkung(?string $einsatzstelle_bemerkung): self
    {
        $this->einsatzstelle_bemerkung = $einsatzstelle_bemerkung;

        return $this;
    }

    public function getKoordinaten(): ?string
    {
        return $this->koordinaten;
    }

    public function setKoordinaten(?string $koordinaten): self
    {
        $this->koordinaten = $koordinaten;

        return $this;
    }

    public function getAdresse(): Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getAlarmStichwort(): string
    {
        return $this->alarm_stichwort;
    }

    public function getAlarmStichwortFuerDatenbank(): string
    {
        return strtoupper(str_replace(' ', '', $this->alarm_stichwort));
    }

    public function setAlarmStichwort(string $alarm_stichwort): self
    {
        $this->alarm_stichwort = $alarm_stichwort;

        return $this;
    }

    public function getMeldebild(): ?string
    {
        return $this->meldebild;
    }

    public function setMeldebild(?string $meldebild): self
    {
        $this->meldebild = $meldebild;

        return $this;
    }

    public function getAlamierungszeit(): DateTimeImmutable
    {
        return $this->alamierungszeit;
    }

    /**
     * @param DateTimeImmutable|DateTime $alamierungszeit
     */
    public function setAlamierungszeit($alamierungszeit): self
    {
        if ($alamierungszeit instanceof DateTime) {
            $alamierungszeit = DateTimeImmutable::createFromMutable($alamierungszeit);
        }
        $this->alamierungszeit = $alamierungszeit;

        return $this;
    }

    public function getBemerkung(): ?string
    {
        return $this->bemerkung;
    }

    public function setBemerkung(?string $bemerkung): self
    {
        $this->bemerkung = $bemerkung;

        return $this;
    }

    public function getMeldender(): ?string
    {
        return $this->meldender;
    }

    public function setMeldender(string $meldender): self
    {
        $this->meldender = $meldender;

        return $this;
    }

    public function getEinsatzmittel(): ?string
    {
        return $this->einsatzmittel;
    }

    public function setEinsatzmittel(?string $einsatzmittel): self
    {
        $this->einsatzmittel = $einsatzmittel;

        return $this;
    }

    public function getEinsatz(): Einsatz
    {
        return $this->einsatz;
    }

    public function setEinsatz(Einsatz $einsatz): self
    {
        $this->einsatz = $einsatz;

        return $this;
    }

    public function getTimestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }
}
