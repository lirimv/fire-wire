<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $menschenLeben = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getMenschenLeben(): bool
    {
        return $this->menschenLeben;
    }

    public function setMenschenLeben(bool $menschenLeben): self
    {
        $this->menschenLeben = $menschenLeben;

        return $this;
    }
}
