<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DecisionRepository")
 */
class Decision
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $AlarmStichwort;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $ort;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $bemerkung;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $decision;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlarmStichwort(): ?string
    {
        return $this->AlarmStichwort;
    }

    public function setAlarmStichwort(string $AlarmStichwort): self
    {
        $this->AlarmStichwort = $AlarmStichwort;

        return $this;
    }

    public function getOrt(): ?string
    {
        return $this->ort;
    }

    public function setOrt(string $ort): self
    {
        $this->ort = $ort;

        return $this;
    }

    public function getBemerkung(): ?string
    {
        return $this->bemerkung;
    }

    public function setBemerkung(string $bemerkung): self
    {
        $this->bemerkung = $bemerkung;

        return $this;
    }

    public function getDecision(): ?string
    {
        return $this->decision;
    }

    public function setDecision(string $decision): self
    {
        $this->decision = $decision;

        return $this;
    }
}
