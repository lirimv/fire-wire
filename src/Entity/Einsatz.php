<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DomainException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EinsatzRepository")
 */
class Einsatz
{
    public const INNERORTS = 'innerorts';
    public const AUSSERORTS = 'ausserorts';
    public const WASSER = 'wasser';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $status;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AlarmNachricht", mappedBy="einsatz", cascade={"persist", "remove"})
     */
    private AlarmNachricht $alarmNachricht;

    public function __construct(AlarmNachricht $alarmNachricht)
    {
        $this->alarmNachricht = $alarmNachricht;
        $this->status = 'offen';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAlarmNachricht(): AlarmNachricht
    {
        return $this->alarmNachricht;
    }

    public function setAlarmNachricht(AlarmNachricht $alarmNachricht): self
    {
        $this->alarmNachricht = $alarmNachricht;

        // set the owning side of the relation if necessary
        if ($alarmNachricht->getEinsatz() !== $this) {
            $alarmNachricht->setEinsatz($this);
        }

        return $this;
    }

    public function getEinsatzOrt(): string
    {
        if ($this->isInnerortsEinsatz()) {
            return self::INNERORTS;
        }

        if ($this->isWasserEinsatz()) {
            return self::WASSER;
        }

        return self::AUSSERORTS;
    }

    public function isInnerortsEinsatz(): bool
    {
        $adresse = $this->alarmNachricht->getAdresse();
        $strasse = $this->alarmNachricht->getAdresse()->getStrasse();

        if ('' === $strasse) {
            return false;
        }

        $hausnummerEmpty = null === $adresse->getHausnummer() || empty($adresse->getHausnummer());
        $match = 1 === preg_match('/straße|weg|str.|feld|ring|am |im |zum/ui', $strasse);

        return  !$hausnummerEmpty && $match;
    }

    public function isWasserEinsatz(): bool
    {
        $adresse = $this->alarmNachricht->getAdresse();

        if (null === $adresse || null === $adresse->getStrasse()) {
            throw new DomainException('Strasse nicht angegeben und kann nicht ausgwertet werden!');
        }

        $strasse = $adresse->getStrasse();

        return
            false === $this->isInnerortsEinsatz()
            && 1 === preg_match('/wasser|see|fluss|fluß|rhein|main|rodau|teich/ui', $strasse);
    }

    public function isAusserortsEinsatz(): bool
    {
        return !$this->isInnerortsEinsatz() && !$this->isWasserEinsatz();
    }
}
