<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StichwortMatrixRepository")
 */
class StichwortMatrix
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $alarmstichwort;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $innerorts;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $ausserorts;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $wasser;

    public function isGesperrt(): bool
    {
        return !$this->isAusserortsErlaubt() && !$this->isWasserErlaubt() && !$this->innerorts;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlarmstichwort(): ?string
    {
        return $this->alarmstichwort;
    }

    public function setAlarmstichwort(string $alarmstichwort): self
    {
        $this->alarmstichwort = $alarmstichwort;

        return $this;
    }

    public function isInnerortsErlaubt(): ?bool
    {
        return $this->innerorts;
    }

    public function setInnerorts(?bool $innerorts): self
    {
        $this->innerorts = $innerorts;

        return $this;
    }

    public function isAusserortsErlaubt(): ?bool
    {
        return $this->ausserorts;
    }

    public function setAusserorts(?bool $ausserorts): self
    {
        $this->ausserorts = $ausserorts;

        return $this;
    }

    public function isWasserErlaubt(): ?bool
    {
        return $this->wasser;
    }

    public function setWasser(?bool $wasser): self
    {
        $this->wasser = $wasser;

        return $this;
    }
}
