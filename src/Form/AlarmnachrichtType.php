<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\AlarmNachricht;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AlarmnachrichtType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('einsatznummer', TextType::class, ['required' => true])
            ->add('einsatzstelle_zusatz', TextType::class, [
                'required' => false,
                'label' => 'Einsatzstelle-Zusatz',
            ])
            ->add('einsatzstelle_bemerkung', TextType::class, [
                'required' => false,
                'label' => 'Einsatzstelle-Bemerkung',
            ])
            ->add('koordinaten', TextType::class, ['required' => false])
            ->add('alarm_stichwort', TextType::class, [
                'required' => true,
                'label' => 'Stichwort',
            ])
            ->add('meldebild', TextareaType::class, ['required' => false])
            ->add('bemerkung', TextareaType::class, ['required' => true])
            ->add('meldender', TextType::class, ['required' => true])
            ->add('einsatzmittel', TextType::class, ['required' => false])
            ->add('adresse', AdresseType::class)
            ->add('submit', SubmitType::class, ['label' => 'Senden'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AlarmNachricht::class,
        ]);
    }
}
