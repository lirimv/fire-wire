<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200222130639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial database';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, strasse VARCHAR(255) NOT NULL, hausnummer VARCHAR(255) NOT NULL, ortsteil VARCHAR(255) NOT NULL, ort VARCHAR(255) NOT NULL, objekt VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alarm_stichwort (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, beschreibung VARCHAR(1024) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE einsatz (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alarm_nachricht (id INT AUTO_INCREMENT NOT NULL, adresse_id INT DEFAULT NULL, einsatz_id INT NOT NULL, einsatznummer VARCHAR(255) NOT NULL, einsatzstelle_zusatz VARCHAR(255) DEFAULT NULL, einsatzstelle_bemerkung VARCHAR(1024) DEFAULT NULL, koordinaten VARCHAR(255) DEFAULT NULL, alarm_stichwort VARCHAR(255) NOT NULL, timestamp DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', meldebild VARCHAR(1024) DEFAULT NULL, alamierungszeit DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', bemerkung VARCHAR(1024) DEFAULT NULL, meldender VARCHAR(255) NOT NULL, einsatzmittel VARCHAR(1024) DEFAULT NULL, INDEX IDX_E18D922B4DE7DC5C (adresse_id), UNIQUE INDEX UNIQ_E18D922B6E1D2EA8 (einsatz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE decision (id INT AUTO_INCREMENT NOT NULL, alarm_stichwort VARCHAR(255) NOT NULL, ort VARCHAR(255) NOT NULL, bemerkung VARCHAR(255) NOT NULL, decision VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE alarm_nachricht ADD CONSTRAINT FK_E18D922B4DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE alarm_nachricht ADD CONSTRAINT FK_E18D922B6E1D2EA8 FOREIGN KEY (einsatz_id) REFERENCES einsatz (id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE alarm_nachricht DROP FOREIGN KEY FK_E18D922B4DE7DC5C');
        $this->addSql('ALTER TABLE alarm_nachricht DROP FOREIGN KEY FK_E18D922B6E1D2EA8');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE alarm_stichwort');
        $this->addSql('DROP TABLE einsatz');
        $this->addSql('DROP TABLE alarm_nachricht');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE decision');
    }
}
