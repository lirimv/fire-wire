<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200222130948 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'stichwort matrix erstellt + fixtures';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE stichwort_matrix (id INT AUTO_INCREMENT NOT NULL, alarmstichwort VARCHAR(255) NOT NULL, innerorts TINYINT(1) DEFAULT NULL, ausserorts TINYINT(1) DEFAULT NULL, wasser TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('
            INSERT INTO firewire.stichwort_matrix (alarmstichwort,innerorts,ausserorts,wasser) VALUES 
            (\'F1\',1,1,NULL)
            ,(\'F2\',0,1,NULL)
            ,(\'F2Y\',0,0,0)
            ,(\'F3\',1,1,NULL)
            ,(\'F3Y\',0,0,0)
            ,(\'F4\',1,1,NULL)
            ,(\'FBAB\',NULL,1,NULL)
            ,(\'FBMA\',0,0,NULL)
            ,(\'FBUSY\',0,0,0)
            ,(\'FFLUG1Y\',0,0,0)
            ;
            INSERT INTO firewire.stichwort_matrix (alarmstichwort,innerorts,ausserorts,wasser) VALUES 
            (\'FFLUG2Y\',0,0,0)
            ,(\'FGAS1\',0,1,NULL)
            ,(\'FGAS2\',1,1,NULL)
            ,(\'FLKW\',1,1,NULL)
            ,(\'FZUG\',0,1,NULL)
            ,(\'FZUGY\',0,0,0)
            ,(\'FRWM\',0,0,NULL)
            ,(\'FSCHIFF1\',NULL,NULL,1)
            ,(\'FSCHIFF2\',NULL,NULL,1)
            ,(\'FSCHIFF2Y\',0,0,0)
            ;
            INSERT INTO firewire.stichwort_matrix (alarmstichwort,innerorts,ausserorts,wasser) VALUES 
            (\'FSCHIFF2GEFAHR\',NULL,NULL,1)
            ,(\'FWALD1\',0,1,NULL)
            ,(\'FWALD2\',1,1,NULL)
            ,(\'H1\',1,1,1)
            ,(\'H1Y\',0,0,0)
            ,(\'H2\',0,1,NULL)
            ,(\'HBAB\',NULL,1,NULL)
            ,(\'HABSICHERUNGRD\',NULL,1,NULL)
            ,(\'HABSTY\',0,0,0)
            ,(\'HELEK\',1,1,NULL)
            ;
            INSERT INTO firewire.stichwort_matrix (alarmstichwort,innerorts,ausserorts,wasser) VALUES 
            (\'HEINSTY\',0,0,0)
            ,(\'HFLUSS\',1,1,NULL)
            ,(\'HFLUSSY\',0,0,0)
            ,(\'HWASSY \',0,0,0)
            ,(\'HGAS1\',0,1,NULL)
            ,(\'HGAS2\',1,1,NULL)
            ,(\'HGEFAHR1\',0,1,NULL)
            ,(\'HGEFAHR2\',1,1,NULL)
            ,(\'HKLEMM1Y\',0,0,0)
            ,(\'HKLEMM2Y\',0,0,0)
            ;
            INSERT INTO firewire.stichwort_matrix (alarmstichwort,innerorts,ausserorts,wasser) VALUES 
            (\'HTIER\',0,1,1)
            ,(\'HÖLFLUSS\',1,1,1)
            ,(\'HÖLWASS\',1,1,1)
            ,(\'HRADIOAKTIV\',1,1,NULL)
            ,(\'HSCHIFF\',NULL,NULL,1)
            ,(\'HSCHIFFY\',0,0,0)
            ,(\'HZUG1Y\',0,0,0)
            ,(\'HZUG2Y\',0,0,0)
            ;
        ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE stichwort_matrix');
    }
}
