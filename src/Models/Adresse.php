<?php

declare(strict_types=1);

namespace App\Models;

use App\Entity\Adresse as AdresseEntity;
use App\Service\Import\Email\Parser;

final class Adresse
{
    private string $ort;
    private string $ortsteil;
    private string $strasse;
    private string $hausnummer;
    private string $objekt;

    private function __construct(string $ort, string $ortsteil, string $strasse, string $hausnummer, string $objekt)
    {
        $this->ort = $ort;
        $this->ortsteil = $ortsteil;
        $this->strasse = $strasse;
        $this->hausnummer = $hausnummer;
        $this->objekt = $objekt;
    }

    /**
     * @param array<string> $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            $data[Parser::ORT] ?? '',
            $data[Parser::ORTSTEIL] ?? '',
            $data[Parser::STRASSE] ?? '',
            $data[Parser::HAUSNR] ?? '',
            $data[Parser::OBJEKT] ?? ''
        );
    }

    public static function fromEntity(AdresseEntity $entity): self
    {
        return new self(
            $entity->getOrt(),
            $entity->getOrtsteil(),
            $entity->getStrasse(),
            $entity->getHausnummer() ?? '',
            $entity->getObjekt() ?? ''
        );
    }

    public function getHausnummer(): string
    {
        return $this->hausnummer;
    }

    public function getObjekt(): string
    {
        return $this->objekt;
    }

    public function getOrt(): string
    {
        return $this->ort;
    }

    public function getOrtsteil(): string
    {
        return $this->ortsteil;
    }

    public function getStrasse(): string
    {
        return $this->strasse;
    }
}
