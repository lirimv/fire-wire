<?php

declare(strict_types=1);

namespace App\Models;

use App\Entity\AlarmNachricht as AlarmNachrichtEntity;
use App\Service\Import\Email\Parser;
use DateTimeImmutable;

/** @psalm-immutable */
final class AlarmNachricht
{
    private string $einsatznummer;
    private string $einsatzstelleZusatz;
    private string $einsatzstelleBemerkung;
    private string $koordinaten;
    private Adresse $adresse;
    private string $alarmStichwort;
    private string $meldebild;
    private DateTimeImmutable $alamierungszeit;
    private string $bemerkung;
    private string $meldender;
    private string $einsatzmittel;

    private function __construct(
        string $einsatznummer,
        string $einsatzstelleZusatz,
        string $einsatzstelleBemerkung,
        string $koordinaten,
        Adresse $adresse,
        string $alarmStichwort,
        string $meldebild,
        DateTimeImmutable $alamierungszeit,
        string $bemerkung,
        string $meldender,
        string $einsatzmittel
    ) {
        $this->einsatznummer = $einsatznummer;
        $this->einsatzstelleZusatz = $einsatzstelleZusatz;
        $this->einsatzstelleBemerkung = $einsatzstelleBemerkung;
        $this->koordinaten = $koordinaten;
        $this->adresse = $adresse;
        $this->alarmStichwort = $alarmStichwort;
        $this->meldebild = $meldebild;
        $this->alamierungszeit = $alamierungszeit;
        $this->bemerkung = $bemerkung;
        $this->meldender = $meldender;
        $this->einsatzmittel = $einsatzmittel;
    }

    /** @param array<string, mixed> $data */
    public static function fromArray(array $data): self
    {
        return new self(
            $data[Parser::EINSATZNUMMER] ?? '',
            $data[Parser::ESTELLEZUSATZ] ?? '',
            $data[Parser::ESTELLEBEMERKUNG] ?? '',
            $data[Parser::KOORDINATE] ?? '',
            Adresse::fromArray($data),
            $data[Parser::STICHWORT] ?? '',
            $data[Parser::MELDEBILD] ?? '',
            isset($data[Parser::AZEIT]) ? new DateTimeImmutable($data[Parser::AZEIT]) : new DateTimeImmutable('now'),
            $data[Parser::BEMERKUNG] ?? '',
            $data[Parser::MELDENDER] ?? '',
            $data[Parser::EINSATZMITTEL] ?? ''
        );
    }

    public static function fromEntity(AlarmNachrichtEntity $entity): self
    {
        return new self(
            $entity->getEinsatznummer(),
            $entity->getEinsatzstelleZusatz(),
            $entity->getEinsatzstelleBemerkung(),
            $entity->getKoordinaten(),
            Adresse::fromEntity($entity->getAdresse()),
            $entity->getAlarmStichwort(),
            $entity->getMeldebild(),
            $entity->getAlamierungszeit(),
            $entity->getBemerkung(),
            $entity->getMeldender(),
            $entity->getEinsatzmittel()
        );
    }

    public function getAdresse(): Adresse
    {
        return $this->adresse;
    }

    public function getAlamierungszeit(): DateTimeImmutable
    {
        return $this->alamierungszeit;
    }

    public function getAlarmStichwort(): string
    {
        return $this->alarmStichwort;
    }

    public function getBemerkung(): string
    {
        return $this->bemerkung;
    }

    public function getEinsatzmittel(): string
    {
        return $this->einsatzmittel;
    }

    public function getEinsatznummer(): string
    {
        return $this->einsatznummer;
    }

    public function getEinsatzstelleBemerkung(): string
    {
        return $this->einsatzstelleBemerkung;
    }

    public function getEinsatzstelleZusatz(): string
    {
        return $this->einsatzstelleZusatz;
    }

    public function getKoordinaten(): string
    {
        return $this->koordinaten;
    }

    public function getMeldebild(): string
    {
        return $this->meldebild;
    }

    public function getMeldender(): string
    {
        return $this->meldender;
    }
}
