<?php

declare(strict_types=1);

namespace App\Models;

use App\Entity\Configuration;

/**
 * @psalm-immutable
 */
final class Config
{
    public string $menschenLeben;

    private function __construct(bool $menschenLeben)
    {
        $this->menschenLeben = true === $menschenLeben ? 'true' : 'false';
    }

    public static function fromEntity(Configuration $configuration): self
    {
        return new self($configuration->getMenschenLeben());
    }
}
