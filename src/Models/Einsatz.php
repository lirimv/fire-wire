<?php

declare(strict_types=1);

namespace App\Models;

use App\Entity\Einsatz as EinsatzEntity;
use App\Service\Validator\Decider;

final class Einsatz
{
    private const GESPERRT_NACHRICHT =
        'Die Feuerwehr Rodgau-Nord befindet sich gerade auf einem Einsatz. Aufgrund des Einsatzgeschehens, werden hier '.
        'keine genaueren Daten angezeigt. Gerne können Sie nach dem Einsatzgeschehen in unserer Einsatzhistorie über '.
        'den Einsatz informieren.';

    private string $status;
    private string $einsatznummer;
    private string $alarmStichwort;
    private string $meldebild;
    private string $strasse;
    private string $objekt;
    private string $einsatzmittel;
    private string $bemerkung;
    private string $alamierungszeit;
    private string $sichtbarkeit;

    private function __construct(
        string $status,
        string $einsatznummer,
        string $alarmStichwort,
        string $meldebild,
        string $bemerkung,
        string $objekt,
        string $einsatzmittel,
        string $strasse,
        string $alamierungszeit,
        string $sichtbarkeit
    ) {
        $this->status = $status;
        $this->einsatznummer = $einsatznummer;
        $this->alarmStichwort = $alarmStichwort;
        $this->meldebild = $meldebild;
        $this->strasse = $strasse;
        $this->objekt = $objekt;
        $this->einsatzmittel = $einsatzmittel;
        $this->bemerkung = $bemerkung;
        $this->alamierungszeit = $alamierungszeit;
        $this->sichtbarkeit = $sichtbarkeit;
    }

    public static function create(string $sichtbarkeit, EinsatzEntity $einsatz): self
    {
        switch ($sichtbarkeit) {
            case Decider::KOMPLETT:
                return self::createKomplett($einsatz);
            case Decider::BEDINGT:
                return self::createBedingt($einsatz);
            default:
                return self::createGesperrt($einsatz);
        }
    }

    public static function createKomplett(EinsatzEntity $einsatz): self
    {
        $alarmnachricht = $einsatz->getAlarmNachricht();
        $adresse = $alarmnachricht->getAdresse();

        return new self(
            $einsatz->getStatus(),
            $alarmnachricht->getEinsatznummer(),
            $alarmnachricht->getAlarmStichwort(),
            $alarmnachricht->getMeldebild() ?? '',
            $alarmnachricht->getBemerkung() ?? '',
            $adresse->getObjekt(),
            $alarmnachricht->getEinsatzmittel() ?? '',
            $adresse->getStrasse(),
            $alarmnachricht->getAlamierungszeit()->format('d.m.Y h:m:s'),
            Decider::KOMPLETT
        );
    }

    public static function createBedingt(EinsatzEntity $einsatz): self
    {
        $alarmnachricht = $einsatz->getAlarmNachricht();

        return new self(
            $einsatz->getStatus(),
            $alarmnachricht->getEinsatznummer(),
            $alarmnachricht->getAlarmStichwort(),
            $alarmnachricht->getMeldebild() ?? '',
            $alarmnachricht->getBemerkung() ?? '',
            '',
            '',
            '',
            $alarmnachricht->getAlamierungszeit()->format('d.m.Y h:m:s'),
            Decider::BEDINGT
        );
    }

    public static function createGesperrt(EinsatzEntity $einsatz): self
    {
        $alarmnachricht = $einsatz->getAlarmNachricht();

        return new self(
            $einsatz->getStatus(),
            $alarmnachricht->getEinsatznummer(),
            '',
            '',
            self::GESPERRT_NACHRICHT,
            '',
            '',
            '',
            $alarmnachricht->getAlamierungszeit()->format('d.m.Y h:m:s'),
            Decider::GESPERRT
        );
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getEinsatznummer(): string
    {
        return $this->einsatznummer;
    }

    public function getMeldebild(): string
    {
        return $this->meldebild;
    }

    public function getEinsatzmittel(): string
    {
        return $this->einsatzmittel;
    }

    public function getAlamierungszeit(): string
    {
        return $this->alamierungszeit;
    }

    public function getStrasse(): string
    {
        return $this->strasse;
    }

    public function getObjekt(): string
    {
        return $this->objekt;
    }

    public function getAlarmStichwort(): string
    {
        return $this->alarmStichwort;
    }

    public function getBemerkung(): string
    {
        return $this->bemerkung;
    }

    public function getSichtbarkeit(): string
    {
        return $this->sichtbarkeit;
    }
}
