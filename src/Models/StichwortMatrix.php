<?php

declare(strict_types=1);

namespace App\Models;

use App\Entity\StichwortMatrix as DomainStichwortMatrix;

/**
 * @psalm-immutable
 */
final class StichwortMatrix
{
    public int $id;
    public string $alarmstichwort;
    public string $innerorts;
    public string $ausserorts;
    public string $wasser;

    private function __construct(int $id, string $alarmstichwort, ?bool $innerorts, ?bool $ausserorts, ?bool $wasser)
    {
        $this->id = $id;
        $this->alarmstichwort = $alarmstichwort;
        $this->innerorts = json_encode($innerorts);
        $this->ausserorts = json_encode($ausserorts);
        $this->wasser = json_encode($wasser);
    }

    public static function fromEntity(DomainStichwortMatrix $matrix): self
    {
        return new self(
            $matrix->getId(),
            $matrix->getAlarmstichwort(),
            $matrix->isInnerortsErlaubt(),
            $matrix->isAusserortsErlaubt(),
            $matrix->isWasserErlaubt()
        );
    }
}
