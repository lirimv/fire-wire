<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AlarmNachricht;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AlarmNachricht|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlarmNachricht|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlarmNachricht[]    findAll()
 * @method AlarmNachricht[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class AlarmNachrichtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlarmNachricht::class);
    }

    public function save(AlarmNachricht $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return AlarmNachricht[] Returns an array of AlarmNachricht objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlarmNachricht
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
