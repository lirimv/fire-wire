<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AlarmStichwort;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AlarmStichwort|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlarmStichwort|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlarmStichwort[]    findAll()
 * @method AlarmStichwort[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class AlarmStichwortRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlarmStichwort::class);
    }

    // /**
    //  * @return AlarmStichwort[] Returns an array of AlarmStichwort objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlarmStichwort
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
