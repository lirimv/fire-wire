<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Einsatz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Einsatz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Einsatz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Einsatz[]    findAll()
 * @method Einsatz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class EinsatzRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Einsatz::class);
    }

    /**
     * @return Einsatz[]
     */
    public function getOffeneEinsaetze(): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.status = :status')
            ->setParameter('status', 'offen')
            ->getQuery()->getResult();
    }

    public function save(Einsatz $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return Einsatz[] Returns an array of Einsatz objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Einsatz
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
