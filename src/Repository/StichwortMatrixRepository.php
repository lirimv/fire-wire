<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\StichwortMatrix;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StichwortMatrix|null find($id, $lockMode = null, $lockVersion = null)
 * @method StichwortMatrix|null findOneBy(array $criteria, array $orderBy = null)
 * @method StichwortMatrix[]    findAll()
 * @method StichwortMatrix[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class StichwortMatrixRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StichwortMatrix::class);
    }

    public function save(StichwortMatrix $matrix): void
    {
        $this->getEntityManager()->persist($matrix);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return StichwortMatrix[] Returns an array of StichwortMatrix objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StichwortMatrix
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
