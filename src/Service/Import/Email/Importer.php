<?php

declare(strict_types=1);

namespace App\Service\Import\Email;

use App\Entity\Adresse;
use App\Entity\AlarmNachricht as AlarmNachrichtEntity;
use App\Models\AlarmNachricht;
use App\Repository\AlarmNachrichtRepository;
use DateTimeImmutable;
use DomainException;
use Webmozart\Assert\Assert;

final class Importer
{
    private AlarmNachrichtRepository $alarmNachrichtRepository;
    private Parser $parser;

    public function __construct(AlarmNachrichtRepository $alarmNachrichtRepository, Parser $parser)
    {
        $this->alarmNachrichtRepository = $alarmNachrichtRepository;
        $this->parser = $parser;
    }

    public function process(string $importDirectory, string $processedDirectory): void
    {
        $alarmNachrichten = $this->parser->parser($importDirectory);

        if ($alarmNachrichten === []) {
            return;
        }

        foreach ($alarmNachrichten as $nachricht) {
            $this->write($nachricht);
            $this->move($importDirectory, $processedDirectory);
        }
    }

    private function write(AlarmNachricht $model): void
    {
        $adresse = new Adresse(
            $model->getAdresse()->getStrasse(),
            $model->getAdresse()->getHausnummer(),
            $model->getAdresse()->getOrtsteil(),
            $model->getAdresse()->getOrt(),
            $model->getAdresse()->getObjekt()
        );

        $alarmNachricht = new AlarmNachrichtEntity($adresse);
        $alarmNachricht->setEinsatznummer($model->getEinsatznummer());
        $alarmNachricht->setEinsatzmittel($model->getEinsatzmittel());
        $alarmNachricht->setMeldebild($model->getMeldebild());
        $alarmNachricht->setEinsatzstelleBemerkung($model->getEinsatzstelleBemerkung());
        $alarmNachricht->setEinsatzstelleZusatz($model->getEinsatzstelleZusatz());
        $alarmNachricht->setAlamierungszeit($model->getAlamierungszeit());
        $alarmNachricht->setKoordinaten($model->getKoordinaten());
        $alarmNachricht->setAlarmStichwort($model->getAlarmStichwort());
        $alarmNachricht->setBemerkung($model->getBemerkung());
        $alarmNachricht->setMeldender($model->getMeldender());

        $this->alarmNachrichtRepository->save($alarmNachricht);
    }

    private function move(string $directory, string $to): bool
    {
        Assert::directory($directory);
        Assert::directory($to);

        $files = glob($directory.'/*.{eml}', GLOB_BRACE);

        if (false === $files) {
            throw new DomainException('Files lost during process');
        }

        $existingFiles = glob($to.'/*', GLOB_BRACE);
        $count = 0;

        if (false !== $existingFiles) {
            $count = count($existingFiles);
        }

        $timestamp = new DateTimeImmutable('now');
        foreach ($files as $file) {
            ++$count;
            rename($file, $to.'/OK_'.$timestamp->format('Y-m-d').'_'.$count);
        }

        return true;
    }
}
