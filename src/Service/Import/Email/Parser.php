<?php

declare(strict_types=1);

namespace App\Service\Import\Email;

use App\Models\AlarmNachricht;
use DomainException;

final class Parser
{
    public const EINSATZNUMMER = 'Einsatznummer';
    public const ORT = 'Ort';
    public const ORTSTEIL = 'Ortsteil';
    public const STRASSE = 'Strasse';
    public const HAUSNR = 'Haus-Nr.';
    public const OBJEKT = 'Objekt.';
    public const ESTELLEZUSATZ = 'E-Stelle-Zusatz';
    public const ESTELLEBEMERKUNG = 'E-Stelle-Bem.';
    public const KOORDINATE = 'Koordinate';
    public const STICHWORT = 'Stichwort';
    public const MELDEBILD = 'Meldebild';
    public const AZEIT = 'Alarmierungszeit';
    public const BEMERKUNG = 'Bemerkung';
    public const MELDENDER = 'Meldender';
    public const EINSATZMITTEL = 'Einsatzmittel';

    /**
     * @return AlarmNachricht[]
     */
    public function parser(string $directory): array
    {
        $files = glob($directory.'/*.{eml}', GLOB_BRACE);

        if (empty($files)) {
            echo 'keine Dateien vorhanden in '.$directory."\n";

            return [];
        }

        $list = [];
        foreach ($files as $file) {
            $content = file($file);

            if (false === $content) {
                throw new DomainException(sprintf('Could not parse files %s', $file));
            }

            $data[self::EINSATZNUMMER] = $this->extractValue(self::EINSATZNUMMER, $content);
            $data[self::ORT] = $this->extractValue(self::ORT, $content);
            $data[self::ORTSTEIL] = $this->extractValue(self::ORTSTEIL, $content);
            $data[self::STRASSE] = $this->extractValue(self::STRASSE, $content);
            $data[self::HAUSNR] = $this->extractValue(self::HAUSNR, $content);
            $data[self::OBJEKT] = $this->extractValue(self::OBJEKT, $content);
            $data[self::ESTELLEZUSATZ] = $this->extractValue(self::ESTELLEZUSATZ, $content);
            $data[self::ESTELLEBEMERKUNG] = $this->extractValue(self::ESTELLEBEMERKUNG, $content);
            $data[self::KOORDINATE] = $this->extractValue(self::KOORDINATE, $content);
            $data[self::STICHWORT] = $this->extractValue(self::STICHWORT, $content);
            $data[self::MELDEBILD] = $this->extractValue(self::MELDEBILD, $content);
            $data[self::AZEIT] = $this->extractValue(self::AZEIT, $content);
            $data[self::BEMERKUNG] = $this->extractValue(self::BEMERKUNG, $content);
            $data[self::MELDENDER] = $this->extractValue(self::MELDENDER, $content);
            $data[self::EINSATZMITTEL] = $this->extractValue(self::EINSATZMITTEL, $content);

            $list[] = AlarmNachricht::fromArray($data);
        }

        return $list;
    }

    /**
     * @param array<mixed, mixed> $file
     */
    private function extractValue(string $key, array $file): string
    {
        $data1 = array_values(preg_grep('/'.$key.'/', $file));

        if (!isset($data1[0])) {
            echo "\n$key nicht gefunden\n";

            return '';
        }

        $value = strstr($data1[0], ':');

        if (false === $value) {
            $value = '';
        }

        if (self::AZEIT == $key) {
            $value = str_replace('Uhr', '', $value);
        }

        return quoted_printable_decode(trim(substr($value, 1)));
    }
}
