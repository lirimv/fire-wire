<?php

declare(strict_types=1);

namespace App\Service\Validator;

use App\Entity\Configuration;
use App\Entity\Einsatz;
use App\Repository\ConfigurationRepository;
use App\Repository\StichwortMatrixRepository;
use DomainException;
use Webmozart\Assert\Assert;

final class Decider
{
    public const KOMPLETT = 'komplett';
    public const BEDINGT = 'bedingt';
    public const GESPERRT = 'gesperrt';

    public const BLACKLIST = ['tot', 'leblos', 'leiche', 'töten'];

    private StichwortMatrixRepository $matrixRepository;
    private ConfigurationRepository $configRepository;

    public function __construct(
        StichwortMatrixRepository $matrixRepository,
        ConfigurationRepository $configRepository
    ) {
        $this->matrixRepository = $matrixRepository;
        $this->configRepository = $configRepository;
    }

    public function decide(Einsatz $einsatz): string
    {
        $alarmnachricht = $einsatz->getAlarmNachricht();
        $config = $this->configRepository->find(1);

        Assert::isInstanceOf($config, Configuration::class);

        if ($config->getMenschenLeben() &&
            $this->isMenschenLebenInGefahr($alarmnachricht->getAlarmStichwort())) {
            return self::GESPERRT;
        }

        $matrix = $this->matrixRepository->findOneBy(['alarmstichwort' => $alarmnachricht->getAlarmStichwortFuerDatenbank()]);
        if (null === $matrix) {
            throw new \Exception('Not found '.$alarmnachricht->getAlarmStichwort());
        }

        if ($matrix->isGesperrt()) {
            return self::GESPERRT;
        }

        if ($einsatz->isInnerortsEinsatz() && !$matrix->isInnerortsErlaubt()) {
            return self::GESPERRT;
        }

        if ($einsatz->isWasserEinsatz() && !$matrix->isWasserErlaubt()) {
            return self::GESPERRT;
        }

        if ($einsatz->isAusserortsEinsatz() && !$matrix->isAusserortsErlaubt()) {
            return self::GESPERRT;
        }

        if (!$this->checkBemerkungen($einsatz)) {
            return self::GESPERRT;
        }

        if ($einsatz->isInnerortsEinsatz()) {
            return self::BEDINGT;
        }

        return self::KOMPLETT;
    }

    private function isMenschenLebenInGefahr(string $stichwort): bool
    {
        return (bool) strpos($stichwort, 'Y');
    }

    private function checkBemerkungen(Einsatz $einsatz): bool
    {
        $bemerkung = $einsatz->getAlarmNachricht()->getBemerkung();

        if (null === $bemerkung) {
            throw new DomainException(sprintf('Bemerkungen konnte nicht ausgewertet werden für Einsatz %s', $einsatz->getId()));
        }

        if (preg_match('/'.implode('|', self::BLACKLIST).'/ui', $bemerkung)) {
            return false;
        }

        return true;
    }
}
